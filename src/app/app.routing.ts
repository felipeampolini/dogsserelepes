import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';

// Import Containers
import {DefaultLayoutComponent} from './containers';

import {P404Component} from './views/error/404.component';
import {P500Component} from './views/error/500.component';
import {LoginComponent} from './views/login/login.component';
import {RegisterComponent} from './views/register/register.component';
import {RecuperarSenhaComponent} from './views/recuperar-senha/recuperar-senha.component';
import {LoginSiteComponent} from './views/login-site/login.component';
import {CadastroComponent} from './views/cadastro/cadastro.component';
import {TelaPrincipalComponent} from './views/tela-principal/tela-principal.component';

export const routes: Routes = [
    { path: 'login-site', component: LoginSiteComponent },
    { path: 'cadastro' , component: CadastroComponent },
    { path: 'recuperar-senha', component: RecuperarSenhaComponent },
    {
        path: '',
        redirectTo: 'login-site',
        pathMatch: 'full',
    },
    {
        path: '404',
        component: P404Component,
        data: {
            title: 'Page 404'
        }
    },
    {
        path: '500',
        component: P500Component,
        data: {
            title: 'Page 500'
        }
    },
    {
        path: '',
        component: DefaultLayoutComponent,
        data: {
            title: "Dog's Serelepe's"
        },
        children: [
            {
                path: 'home',
                loadChildren: './views/tela-principal/tela-principal.module#TelaPrincipalModule',
            },
            {
                path: 'editar-perfil-cuidador',
                loadChildren: './views/editar-perfil-cuidador/editar-perfil-cuidador.module#EditarPerfilCuidadorModule'
            },
            {
                path: 'editar-perfil-cliente',
                loadChildren: './views/editar-perfil-cliente/editar-perfil-cliente.module#EditarPerfilClienteModule'
            },
            {
                path: 'cadastrar-animal',
                loadChildren: './views/cadastrar-animal/cadastrar-animal.module#CadastrarAnimalModule'
            },
            {
                path: 'configuracao',
                loadChildren: './views/configuracao/configuracao.module#ConfiguracaoModule'
            },
            {
                path: 'denunciar-abuso',
                loadChildren: './views/denunciar-abuso/denunciar-abuso.module#DenunciarAbusoModule'
            },
            {
                path: 'buttons',
                loadChildren: './views/buttons/buttons.module#ButtonsModule'
            },
            {
                path: 'charts',
                loadChildren: './views/chartjs/chartjs.module#ChartJSModule'
            },
            {
                path: 'dashboard',
                loadChildren: './views/dashboard/dashboard.module#DashboardModule'
            },
            {
                path: 'icons',
                loadChildren: './views/icons/icons.module#IconsModule'
            },
            {
                path: 'notifications',
                loadChildren: './views/notifications/notifications.module#NotificationsModule'
            },
            {
                path: 'theme',
                loadChildren: './views/theme/theme.module#ThemeModule'
            },
            {
                path: 'widgets',
                loadChildren: './views/widgets/widgets.module#WidgetsModule'
            }
        ]
    },
    {path: '**', component: P404Component}
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {
}

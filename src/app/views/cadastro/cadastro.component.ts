import {Component, OnInit, ViewContainerRef} from '@angular/core';
import {Cep} from '../../models/cep';
import {Usuario} from '../../models/usuario';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MustMatch} from '../../_helpers/must-match.validator';
import {ToastrService} from 'ngx-toastr';


@Component({
    selector: 'app-cadastro',
    templateUrl: './cadastro.component.html',
    styleUrls: ['./cadastro.component.css']
})
export class CadastroComponent implements OnInit {

    public usuario = new Usuario();
    public data;
    public dataatual = new Date();
    public endereco = new Cep();
    public cep;
    public numero;
    public rua;
    public complemento;
    registerForm: FormGroup;

    submitted = false;

    constructor(private formBuilder: FormBuilder, private toastr: ToastrService) {
    }

    get f() {
        return this.registerForm.controls;
    }

    voltar() {
        window.location.href = '/#/login-site';
    }

    onSubmit() {
        this.submitted = true;
        // stop here if form is invalid
        if (this.registerForm.invalid) {
            return;
        } else {
            this.toastr.success('Cadastro realizado com sucesso!', 'Sucesso!');
            setTimeout(function () {
                window.location.href = '/#/home';
            }, 2000);

        }
    }


    ngOnInit() {
        const dia = this.dataatual.getDate();
        let dia2;
        if (dia < 10) {
            dia2 = '0' + dia;
        } else {
            dia2 = dia;
        }
        const mes = this.dataatual.getMonth() + 1;
        const ano = this.dataatual.getFullYear();
        if (mes === 10 || mes === 11 || mes === 12) {
            this.data = ano + '-' + mes + '-' + dia2;
        } else {
            this.data = ano + '-0' + mes + '-' + dia2;
        }

        this.registerForm = this.formBuilder.group({
            nome: ['', Validators.required],
            email: ['', [Validators.required, Validators.email]],
            password: ['', [Validators.required, Validators.minLength(6)]],
            confirmPassword: ['', Validators.required],
            cpf: ['', [Validators.required, Validators.minLength(14)]],
            data_nascimento: ['', Validators.required],
            celular: ['', [Validators.required, Validators.minLength(15)]]
        }, {
            validator: MustMatch('password', 'confirmPassword')
        });

    }

}

import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {CadastrarAnimalComponent} from './cadastrar-animal.component';
import {CadastrarAnimalRoutingModule} from './cadastrar-animal-routing.module';

@NgModule({
    declarations: [
        CadastrarAnimalComponent
    ],
    imports: [
        CommonModule,
        CadastrarAnimalRoutingModule
    ]
})
export class CadastrarAnimalModule {
}

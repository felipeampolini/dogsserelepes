import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CadastrarAnimalComponent } from './cadastrar-animal.component';

const routes: Routes = [
    {
        path: '',
        component: CadastrarAnimalComponent,
        data: {
            title: 'Cadastrar Animal'
        }
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class CadastrarAnimalRoutingModule {}


import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DenunciarAbusoComponent } from './denunciar-abuso.component';

const routes: Routes = [
    {
        path: '',
        component: DenunciarAbusoComponent,
        data: {
            title: 'Denunciar Abuso'
        }
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class DenunciarAbusoRoutingModule {}


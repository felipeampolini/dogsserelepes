import {Component, OnInit} from '@angular/core';

@Component({
    selector: 'app-denunciar-abuso',
    templateUrl: './denunciar-abuso.component.html',
    styleUrls: ['./denunciar-abuso.component.scss']
})
export class DenunciarAbusoComponent implements OnInit {

    constructor() {
    }

    public usuarios = [
        {name: 'João Padilha'},
        {name: 'Mario Lima'},
        {name: 'Denilson Abreu'},
        {name: 'Gustavo Aguiar'},
        {name: 'Alexandre Bernardi'},
        {name: 'Felipe Ampolini'},
        {name: 'Gabriel Almeida'},
        {name: 'Pablo Schena'},
        {name: 'Marco Antonio'}
    ];

    ngOnInit() {
    }

}

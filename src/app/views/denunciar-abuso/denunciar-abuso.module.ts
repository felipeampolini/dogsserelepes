import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {DenunciarAbusoComponent} from './denunciar-abuso.component';
import {DenunciarAbusoRoutingModule} from './denunciar-abuso-routing.module';
import {NgSelectModule} from '@ng-select/ng-select';
import { FormsModule } from '@angular/forms';

@NgModule({
    declarations: [
        DenunciarAbusoComponent
    ],
    imports: [
        CommonModule,
        DenunciarAbusoRoutingModule,
        NgSelectModule,
        FormsModule
    ]
})
export class DenunciarAbusoModule {
}

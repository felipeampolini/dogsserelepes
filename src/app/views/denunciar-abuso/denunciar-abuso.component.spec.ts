import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DenunciarAbusoComponent } from './denunciar-abuso.component';

describe('DenunciarAbusoComponent', () => {
  let component: DenunciarAbusoComponent;
  let fixture: ComponentFixture<DenunciarAbusoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DenunciarAbusoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DenunciarAbusoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import {Component, OnInit} from '@angular/core';
import {Userpadrao1} from '../../models/userspadrao';
import {Usuario} from '../../models/usuario';
import {ToastrService} from 'ngx-toastr';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.css']
})
export class LoginSiteComponent implements OnInit {

    public userPadrao = new Userpadrao1();
    public usuario = new Usuario();
    registerForm: FormGroup;
    registerForm2: FormGroup;

    submitted = false;
    constructor(private toastr: ToastrService, private formBuilder: FormBuilder) {
    }

    get f() {
        return this.registerForm.controls;
    }

    onSubmit() {
        this.submitted = true;
        // stop here if form is invalid
        // return window.location.href = '#/home';
        if (this.registerForm.invalid) {
            return;
        }
        if (this.usuario.email !== this.userPadrao.email || this.usuario.senha !== this.userPadrao.senha) {
            this.toastr.error('Email ou Senha inválidos', 'Erro!');
        } else {
            return window.location.href = '#/home';
        }
    }

    ngOnInit() {
        this.registerForm = this.formBuilder.group({
            email: ['', [Validators.required, Validators.email]],
            senha: ['', [Validators.required, Validators.minLength(6)]],
        });
    }

}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EditarPerfilCuidadorComponent} from './editar-perfil-cuidador.component';
import { EditarPerfilCuidadorRoutingModule} from './editar-perfil-cuidador-routing.module';

@NgModule({
  declarations: [
    EditarPerfilCuidadorComponent
  ],
  imports: [
    CommonModule,
    EditarPerfilCuidadorRoutingModule
  ]
})
export class EditarPerfilCuidadorModule { }

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import {EditarPerfilCuidadorComponent} from './editar-perfil-cuidador.component';

const routes: Routes = [
    {
        path: '',
        component: EditarPerfilCuidadorComponent,
        data: {
            title: 'Editar Perfil Cuidador'
        }
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class EditarPerfilCuidadorRoutingModule {}


import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EditarPerfilClienteComponent} from './editar-perfil-cliente.component';
import { EditarPerfilClienteRoutingModule} from './editar-perfil-cliente-routing.module';

@NgModule({
  declarations: [
    EditarPerfilClienteComponent
  ],
  imports: [
    CommonModule,
    EditarPerfilClienteRoutingModule
  ]
})
export class EditarPerfilClienteModule { }

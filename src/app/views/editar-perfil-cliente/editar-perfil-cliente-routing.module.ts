import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import {EditarPerfilClienteComponent} from './editar-perfil-cliente.component';

const routes: Routes = [
    {
        path: '',
        component: EditarPerfilClienteComponent,
        data: {
            title: 'Editar Perfil Cliente'
        }
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class EditarPerfilClienteRoutingModule {}


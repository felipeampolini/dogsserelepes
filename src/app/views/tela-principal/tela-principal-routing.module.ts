import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TelaPrincipalComponent } from '../tela-principal/tela-principal.component';

const routes: Routes = [
    {
        path: '',
        component: TelaPrincipalComponent,
        data: {
            title: 'Home'
        }
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class TelaPrincipalRoutingModule {}


import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {TelaPrincipalComponent} from './tela-principal.component';
import {TelaPrincipalRoutingModule} from './tela-principal-routing.module';
import {ToastrModule} from 'ngx-toastr';

@NgModule({
  declarations: [
      TelaPrincipalComponent
  ],
  imports: [
    ToastrModule.forRoot({
      positionClass: 'toast-top-right'
    }),
    CommonModule,
      TelaPrincipalRoutingModule
  ]
})
export class TelaPrincipalModule { }

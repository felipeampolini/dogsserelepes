import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ConfiguracaoComponent} from './configuracao.component';
import {ConfiguracaoRoutingModule} from './configuracao-routing.module';

@NgModule({
    declarations: [
        ConfiguracaoComponent
    ],
    imports: [
        CommonModule,
        ConfiguracaoRoutingModule
    ]
})
export class ConfiguracaoModule {
}

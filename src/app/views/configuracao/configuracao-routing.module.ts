import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ConfiguracaoComponent } from './configuracao.component';

const routes: Routes = [
    {
        path: '',
        component: ConfiguracaoComponent,
        data: {
            title: 'Configuração'
        }
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ConfiguracaoRoutingModule {}


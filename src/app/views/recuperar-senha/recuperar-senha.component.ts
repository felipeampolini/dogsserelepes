import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Usuario} from '../../models/usuario';
import {ToastrService} from 'ngx-toastr';

@Component({
    selector: 'app-recuperar-senha',
    templateUrl: './recuperar-senha.component.html',
    styleUrls: ['./recuperar-senha.component.css']
})
export class RecuperarSenhaComponent implements OnInit {

    public usuario = new Usuario();
    registerForm: FormGroup;

    submitted = false;

    constructor(private formBuilder: FormBuilder, private toastr: ToastrService) {
    }

    voltar() {
        window.location.href = '/#/login-site';
    }

    onSubmit() {
        this.submitted = true;
        // stop here if form is invalid
        if (this.registerForm.invalid) {
            return;
        } else {
            this.toastr.success('Email enviado com sucesso!', 'Sucesso!');
            setTimeout(function () {
                window.location.href = '/';
            }, 2000);

        }
    }


    get f() {
        return this.registerForm.controls;
    }

    ngOnInit() {
        this.registerForm = this.formBuilder.group({
            email: ['', [Validators.required, Validators.email]],
        });
    }

}

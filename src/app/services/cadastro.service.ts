import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';



@Injectable()
export class CadastroService {
    constructor(private http: HttpClient) {
    }

    getEndereco(cep) {
        return this.http.get(`https://viacep.com.br/ws/${cep}/json/`);
    }
}

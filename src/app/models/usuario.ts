export class Usuario {
    nome: string;
    senha: string;
    email: string;
    cpf: string;
    aniversario: string;
    celular: string;
    avaliação: number;
}
